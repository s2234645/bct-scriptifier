# bct-scriptifier

Group project for Blockchain Technology course

### Gitlab team working flow
1.  Lets say you are a new team member and you don't have the project source code.  So first clone develop branch of the project to your local machine(I hope you know where to go )

		git clone -b develop https://gitlab.utwente.nl/s2234645/bct-scriptifier.git
2. develop branch is our reference project.  it is recommended always to work on your local branch and create merge request. create your local branch with name like 'chen-dev or man-dev or tim-dev'.   you have to be in develop branch when you execute this git command.

		git checkout -b chen-dev
3. No you are in your new branch( you can check that using git branch)

		git add. 
		git commit -m "your message" 

	go to develop branch to pull if there are new changes pushed by someon. 

		git checkout develop 
		git pull origin develop 

	go back to your local branch and merge develop

		git checkout chen-dev
		git merge develop

	Now you can push your commits and create merge request 
	
		git push origin chen-dev
		
	got to gitlab and create git merge request. don't forget to change the target branch to develop, by default it is main
	
	At this point you can go to step 3 and repeat the process. 
------------
